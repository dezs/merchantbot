
var infoResolver = require("./infoResolver")
var lev = require("../recognise/lev")

var provider = {}

var fields = ["description", "usage", "skills", "passive", "gearing", "bis", "gear", "ability", "abilities", "how", "howto", "how to get"]

var synonims = {
    "gear": "gearing",
    "ability": "skills",
    "abilities": "skills",
    "how": "how to get", 
    "howto": "how to get",     
}

provider.getInfo = function (part1, part2, part3) {
    var havePart3 = part3.length > 0;
    var resolved = infoResolver.resolve(part1, part2);
    //console.log("get info called")
    if (!resolved.isResolved) {
        return `Cannot understand class name "${part1}". Available classes: ${infoResolver.classes.join(", ")}`;
    }

    var newPart2 = resolved.isSecondPartUsed ? part3 : part2;
    if (newPart2) {
        var p2 = newPart2.toLowerCase();
        if (p2 && !resolved.info[p2]) {
            p2 = lev.findClosest(p2, fields, 3).match;
            if (p2 != null) {
                p2 = lev.replaceSynonim(p2, synonims);
            }
        }

        if (p2 && resolved.info[p2]) {
            var category = p2.charAt(0).toUpperCase() + p2.slice(1);
            return `__**${resolved.className} ${category}**__
${resolved.info[p2]}`;
        }
    }

    var text = `__**${resolved.className}**__
${resolved.info.description}
__**Usage**__
${resolved.info.usage}`
    if (resolved.info.skills) {
        text = text + `
__**Skills**__
${resolved.info.skills}`;
    }
    if (resolved.info.passive) {
        text = text + `
__**Passive choise**__
${resolved.info.passive}`;
    }
    if (resolved.info.gearing) {
        text = text + `
__**Gearing**__
${resolved.info.gearing}`;
    }
    if (resolved.info["how to get"] && resolved.info["how to get"] != "Unlocked by default.") {
        text = text + `
__**How to unlock**__
${resolved.info["how to get"]}`;
    }
    if (resolved.info.bis) {
        text = text + `
__**Best in Slot**__
${resolved.info.bis}`;
    }
    return text
}

module.exports = provider