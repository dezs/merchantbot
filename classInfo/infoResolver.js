//98 v

var resolver = {}

var lev = require("../recognise/lev")

resolver.classes = [
    "rogue", "mage", "cleric", "warrior", "berserker", "assassin", "paladin", "dark knight", "bard"
]

resolver.info = {}
resolver.info["rogue"] = {
    description: "A nimble fighter with high accuracy.",
    usage: "Damage dealer and tank.",
    skills: `**Cheap Shot**, **Cheap Shot II** - true damage.
**Evade** - use when boss are going to perform strong attack.
**Sunder** - nice to use if you gear **Berserker**/ not true damage **Assassin** as main damage dealer
**Ambush** - lower enemy atk, useful against high atk bosses`,
    passive: "**Might** if have more atk, **Keen Eye** if more acc.",
    gearing: "Maximize **Atk** and **acc**.",
    bis: "http://merchantrpg.wikia.com/wiki/Rogue/BIS",
    "how to get": "Unlocked by default."
}
resolver.info["mage"] = {
    description: "A powerful wizard with high magic attack.",
    usage: "Silence on group bosses, magic damage dealer.",
    skills: `**Mind Blast** - damage ability.
**Magic Missile** + **Mind Blast II** x 2 - best dps rotation for bosses.
**Silence** - remove strong buffs from bosses. **Cannot miss**.
**Mana Shield** - for tanking/solo matk enemies.
**Amplify Magic** - DLC skill, doubles effect of current debuffs on target.`,
    passive: "**Intellect** most of times. Switch to **Wisdom** when you need few AP to cast silence/etc",
    gearing: "Use gear which give more **MAtk**, but keep **Acc** >= enemy **Eva**.",
    bis: "http://merchantrpg.wikia.com/wiki/Mage/BIS",
    "how to get": "Unlocked by default."
}
resolver.info["cleric"] = {
    description: "A wise priest with high accuracy.",
    usage: "Healing!",
    skills: `**Mind Blast** - standard damage ability.
**Divine Smite** - nice damage and boost for acc&matk party
**Lesser Heal**, **Greater Heal** - healing for solo target/ all party. **Cannot crit**.
**Cleanse** - remove debuffs from party. Useful in few situations (i.e. weekly boss **Hel**).`,
    passive: "**Intellect** is best choise in most cases. **Wisdom** is useful if you need 1-2 AP to cast heal",
    gearing: "Use gear which give more **MAtk**.",
    bis: "http://merchantrpg.wikia.com/wiki/Cleric/BIS",
    "how to get": "Achieve level 15 Mage."
}
resolver.info["warrior"] = {
    description: "A strong tank with high defense.",
    usage: "Pdef Tank.",
    skills: `**Block** - tank physical damage easily.
**Taunt** - new skill added in TFT. Protect your damage dealers with this. Can taunt AOE skills`,
    bis: "http://merchantrpg.wikia.com/wiki/Warrior/BIS",
    "how to get": "Unlocked by default."
}
resolver.info["berserker"] = {
    description: "A mighty warrior with high attack.",
    usage: "Physical damage dealer.",
    skills: `**Slash** - standard damage ability 
**Enrage** - good atk boost for boss fights
**Vicious Strike** - another atk boost, stacks with **Enrage**
**Vicious Strike II** - cool with high crit for boss fights
**Blind Fury** - huge crit boost, use with DLC skill  **Berserk Smash**
**Berserk Smash** - DLC skill, high damage against end-game bosses.`,
    passive: "**Might** is best choise in most cases.",
    bis: "http://merchantrpg.wikia.com/wiki/Berserker/BIS",
    "how to get": "Achieve level 15 Warrior."
}
resolver.info["assassin"] = {
    description: "A deadly ninja with high crit.",
    usage: `Damage dealer. With t5/t6 gear two builds are possible: 
Max acc: using **Puncture** for true damage
Acc/CDmg + 100 crit: using **Kidney Shot**, this build shines with bard's **Poem of Focus**`,
    bis: "http://merchantrpg.wikia.com/wiki/Assassin/BIS",
    "how to get": "Achieve level 15 Rogue.",
    passive: "**Keen Eye** for true dmg build, **Critical Strike** for acc+crit",
}
resolver.info["paladin"] = {
    description: "A sturdy tank with high magic defense.",
    usage: "Mdef Tank for early and mid game. AOE shield (lvl 51 ability) is useful for end-game magic bosses",
    bis: "http://merchantrpg.wikia.com/wiki/Paladin/BIS",
    "how to get": "Achieve level 30 Warrior and level 15 Cleric."
}
resolver.info["dark knight"] = {
    description: "A powerful warlock with high damage.",
    usage: "Damage dealer and debuffer.",
    bis: "http://merchantrpg.wikia.com/wiki/Dark_Knight/BIS",
    "how to get": "Achieve level 30 Mage and level 15 Berserker."
}
resolver.info["bard"] = {
    description: "Support for party bosses.",
    usage: `Use it for 6-man party to maximize profit.
**Poem of Focus** is a huge boost for crit-builded party`,
    bis: "It's support class, so **AP**, **speed** and **luck** gear is good",
    "how to get": "Buy The Frozen Tome expansion."
}

var synonims = {
    "war": "warrior",
    "cler": "cleric",
    "zerk": "berserker",
    "sin": "assassin",
    "pally": "paladin",
    "pal":"paladin",
    "dk": "dark knight",
    "priest": "cleric"
}

var classNames = resolver.classes.slice()

for (let syn in synonims) {
    classNames.push(syn)
}

resolver.tryToResolveClass = function (loweredName, loweredName2) {
    var haveSecondName = loweredName2.length > 0;
    var isSecondPartUsed = haveSecondName;
    var name1 = haveSecondName ? loweredName + ' ' + loweredName2 : loweredName;
    var name2 = haveSecondName ? loweredName : null;
    var classIdx = classNames.indexOf(name1);
    if (classIdx == -1 && haveSecondName) {
        classIdx = classNames.indexOf(name2);
        isSecondPartUsed = false;
    }
    if (classIdx > -1) {
        return {
            isResolved: true,
            name: lev.replaceSynonim(classNames[classIdx], synonims),
            isSecondPartUsed: isSecondPartUsed
        };
    }

    if (name1.length < 20) {
        var test = lev.findClosest(name1, classNames);
        if (haveSecondName) {
            var test2 = lev.findClosest(name2, classNames);
            if (test2.distance < test.distance) {
                test = test2;
                isSecondPartUsed = false;
            }
        }
        if (test.match != null) {
            return {
                isResolved: true,
                name: lev.replaceSynonim(test.match, synonims),
                isSecondPartUsed: isSecondPartUsed
            };
        }
    }

    return {
        isResolved: false,
        isSecondPartUsed: false
    };
}

resolver.resolve = function (classIncoming, part2) {
    var result = resolver.tryToResolveClass(classIncoming.toLowerCase(), part2.toLowerCase())
    if (result.isResolved == false) {
        return {
            isResolved: false
        }
    }
    var classLowered = result.name;
    var className = classLowered.charAt(0).toUpperCase() + classLowered.slice(1);

    return {
        isResolved: true,
        className: className,
        info: resolver.info[classLowered],
        isSecondPartUsed: result.isSecondPartUsed
    }
}

module.exports = resolver