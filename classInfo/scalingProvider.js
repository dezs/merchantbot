var classInfoResolver = require("./infoResolver")

var provider =  {}

var heroesData = require("../data/Heroes.json")
var scalinginfo = {}
for (let i = 0; i < heroesData.length; i++) {
    let currentHeroData = heroesData[i];
    let currentHeroName = currentHeroData.name.toLowerCase();
    scalinginfo[currentHeroName] = {
        normal: {
            str: currentHeroData.strMod[0],
            int: currentHeroData.intMod[0],
            dex: currentHeroData.dexMod[0],
        },
        ascended: {
            str: currentHeroData.strMod[1],
            int: currentHeroData.intMod[1],
            dex: currentHeroData.dexMod[1],
        },
        ascended2: {
            str: currentHeroData.strMod[2],
            int: currentHeroData.intMod[2],
            dex: currentHeroData.dexMod[2],
        },
    }
}

const scalingTable = "```Markdown" + `
!!! outdated !!!
|    Class    | Str | Int | Dex | Asc  Str | Int | Dex |
|:-----------:|:---:|:---:|:---:|:--------:|:---:|:---:|
|    Rogue    |  4  |  1  |  5  |     6    |  1  |  8  |
|     Mage    |  1  |  6  |  3  |     1    |  9  |  5  |
|   Warrior   |  5  |  1  |  4  |     7    |  1  |  6  |
|  Berserker  |  6  |  1  |  3  |     9    |  1  |  5  |
|    Cleric   |  1  |  5  |  4  |     1    |  8  |  6  |
|   Assassin  |  3  |  1  |  6  |     5    |  1  |  7  |
|   Paladin   |  3  |  3  |  3  |     5    |  5  |  5  |
| Dark Knight |  5  |  5  |  0  |     8    |  8  |  1  |
` + "```"



provider.getInfo = function (part1, part2) {
    var result = classInfoResolver.tryToResolveClass(part1.toLowerCase(), part2.toLowerCase());
    var name = result.name;
    if (result.isResolved == false || scalinginfo[name] == undefined) {
        return scalingTable;
    }

    var sc = scalinginfo[name];
    return `__**${name.charAt(0).toUpperCase() + name.slice(1)}**__
No ascension - **${sc.normal.str}** atk per **str**, **${sc.normal.int}** matk per **int**, **${sc.normal.dex}** acc per **dex**
Ascended Once - **${sc.ascended.str}** atk per **str**, **${sc.ascended.int}** matk per **int**, **${sc.ascended.dex}** acc per **dex**
Ascended Twice - **${sc.ascended2.str}** atk per **str**, **${sc.ascended2.int}** matk per **int**, **${sc.ascended2.dex}** acc per **dex**
    `
}

module.exports = provider;