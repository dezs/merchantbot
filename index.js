//ping part (for glitch hosting)
const express = require('express');
const moment = require('moment');
const app = express();
app.get("/", (request, response) => {
    var resText = moment().format('H:mm:ss') + " Ping Received"
    console.log(resText);
    response.send(resText);
});
app.listen(process.env.PORT || 8000);
//ping part ends



var Discord = require('discord.js');
var utility = require("./utility")
var config = {
    //current token can be found on https://discordapp.com/developers/applications/me
    //to authorize new token use:
    //https://discordapp.com/api/oauth2/authorize?client_id=355309176098127873&scope=bot&permissions=0
    token: process.env.TOKEN
}
//require("./secretConfig")

var bot = new Discord.Client();
console.log(moment().format('H:mm:ss') + " bot created")

var serverData = require("./serverData")

bot.on('ready', function () {
    console.log(moment().format('H:mm:ss') + ' Logged in as %s - %s\n', bot.user.username, bot.user.id);

    serverData.init(bot)
});

bot.on('disconnect', function (closeEvent) {
    console.log(moment().format('H:mm:ss') + " disconnect connection", closeEvent.code, closeEvent) //https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent
  
    bot.login(config.token);
});

bot.on('error', function (error) {
    console.log(moment().format('H:mm:ss') + " error connection", error.message, error)
  
  
    bot.login(config.token);
});

function endReply(func, replyObj, message) {
    if (!replyObj) return;
    if (replyObj.isRestricted) {
        message.reply(`Please, use ${serverData.channels.botPlayground} for bot commands`)
        message.delete(200)
            .then(msg => console.log(`Deleted message from ${msg.author.username}`))
            .catch(console.error);
        return
    }
    if (replyObj.msg) {
        func(replyObj.msg)
        return
    }
    if (replyObj.promise) {
        replyObj.promise.then(msg => func(msg), err => { })
    }
}

bot.on('message', function (message) {
    if (message.system) {
        return;
    }
    var user = message.author
    var userID = user.id
    var channel = message.channel

    //console.log("message received", message, user, channel)

    if (message.guild && message.guild.name != "Retora Games") { //allow PM messages
        console.log("message from wrong server", message.guild ? message.guild.name : "<null>", channel)
        return
    }

    var mentionState = utility.getMentionState(message, bot);
    if (mentionState == 2) {
        console.log("mentioned everyone")
        return
    }

    var isChannelRestricted = channel && serverData.restrictedChannels.indexOf(channel) > -1;

    if (mentionState == 1) {
        var botMention = "<@" + bot.user.id + ">";
        var messageCorrected = message.content.replace(botMention, "").trim().toLowerCase();
        var reply = utility.resolveMentionMessage(messageCorrected, message, isChannelRestricted)
        //console.log("reply for mention")

        //if (reply.doPM){            
        //    message.reply("not implemented")
        //}else{
        if (reply.msg || reply.isRestricted) {
            endReply(function (msg) {
                console.log("reply for mention", user.username)
                message.reply(msg)
            }, reply, message)
        } else if (messageCorrected.indexOf("good bot") > -1) {
            message.reply("\:heart:")
        } else if (messageCorrected.indexOf("good boy") > -1) {
            message.reply("You too")
        }
        return
    }

    var reply = utility.resolveCommonMessage(message.content, message, isChannelRestricted)
    endReply(function (msg) {
        console.log(moment().format('H:mm:ss') + " reply in chat for", user.username)
        channel.send(msg)
    }, reply, message)
});

bot.on('guildMemberAdd', member => {
    const channel = serverData.channels.intro;
    if (!channel) {
        console.log("introduction channel not found!")
        return;
    }
    channel.send(`${member}, Welcome to the Server!
Please introduce yourself.

Feel free to ask questions in ${serverData.channels.questionsChannel}.
For bot commands use ${serverData.channels.botPlayground}. Type !help to see available commands.`);
});

bot.on('guildMemberRemove', member => {
    const channel = serverData.channels.botTest;
    if (!channel) {
        console.log("bot-test channel not found");
        return
    }
    let user = member.user
    console.log(`member leave. username - ${user.username}. Tag - ${user.tag}`)
    channel.send(`${user.tag} has left the server`)

})

bot.login(config.token);

