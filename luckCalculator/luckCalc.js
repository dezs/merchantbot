var data = require("./questData")
var lev = require("../recognise/lev")

var calc = {}

calc.getLuck = function (questName) {
    var lowerName = questName.toLowerCase();
    var quest = data.data.find(x => x.nameLower == lowerName);

    if (quest == undefined) {
        var matchQuest = lev.findClosest(questName, data.data, 5, function (qData) { return qData.nameLower })
        if (matchQuest == undefined) {
            return `Quest ${questName} not found`;
        }
        quest = matchQuest.match
    }

    var result = `__**Luck cap for quest ${quest.name}**__
`;
    var res;
    if (quest.loot1) {
        try {
            var min = quest.loot1.min;
            res = Math.ceil(10 * (4.5 - min * 1.5)) + " for 5x " + quest.loot1.material;
        } catch (ex) {
            console.log("error on loot1", ex)
            res = "error"
        }
        result += `**Slot 1** - ${res}
`
    }
    if (quest.loot2) {
        try {
            var min = quest.loot2.min;
            res = Math.ceil(10 * (4.5 - min * 1.5)) + " for 5x " + quest.loot2.material;
        } catch (ex) {
            console.log("error on loot2", ex)
            res = "error"
        }
        result += `**Slot 2** - ${res}
`
    }
    if (quest.loot3) {
        try {
            var min = quest.loot3.nilOdds;
            var maxChance = Math.round(1e4 * quest.loot3.normalOdds / (quest.loot3.normalOdds + 1)) / 100
            res = Math.max(Math.ceil(min * 0.75) - 1, 0) + ` (up to ${maxChance}% chance)`;
        } catch (ex) {
            console.log("error on loot3", ex)
            res = "error"
        }
        result += `**Slot 3** - ${res}
`
    }
    if (quest.loot4) {
        try {
            var min = quest.loot4.nilOdds;
            var maxChance = Math.round(1e4 * quest.loot4.normalOdds / (quest.loot4.normalOdds + 1)) / 100
            res = Math.max(Math.ceil(min * 0.75) - 1, 0) + ` (up to ${maxChance}% chance)`;
        } catch (ex) {
            console.log("error on loot4", ex)
            res = "error"
        }
        result += `**Slot 4** - ${res}
`
    }

    return result;
}

calc.getQuestName = function (id) {
    var quest = data.data.find(x => x.id == id);
    if (quest == undefined) return "<undefined>"
    return quest.name
}

calc.getQuestById = function(id){
    var quest = data.data.find(x => x.id == id);
    if (quest == undefined) return null
    return quest
    
}

//console.log("Demon Warrior luck",calc.getLuck("Demon Warrior"));

module.exports = calc