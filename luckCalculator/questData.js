var data = {
    data: []
}

var quests = require("../data/QuestList.json")
var parcels = require("../data/ParcelList.json")
var materials = require("../data/MaterialList.json")
//console.log("quests", quests.length)
//console.log("parcels", parcels.length) parcels is object

function loadAndFilterData(destination) {
    //json data taken from https://github.com/Dave-Hughes/MerchantGameDB/blob/master/json/stable/QuestList.json
    let allData = quests;
    let skiped = 0;
    allData.forEach((val, index) => {
        if (val.nickname != "MissNo" && val.region >= 1 && val.region <= 6) {
            //let parced = 
            //val.searchName = val.name
            destination.push(convertModel(val, index));
            if (val.nameB != undefined) {
                destination.push(convertModel(val, index, true));
            }
        } else {
            skiped++;
            //console.log("skip", val)
        }
    });
    console.log("enemy load complete, skiped " + skiped + " enemies vv");
}

function processLoot12(loot, name) {
    if (loot == undefined) {
        return loot;
    }
    var material = materials[loot[0] - 1]
    if (material) {
        material = material.name
    } else {
        console.log("undefined material", loot[0], name)
    }

    return {
        material: material,
        min: loot[1],
        max: loot[2]
    }
}

function processLoot34(loot) {
    if (loot == undefined || loot == "undefined") {
        return loot;
    }
    if (parcels[loot] == undefined) {
        //console.log("loot undefined", loot, parcels[loot])
        return parcels[loot];
    }
    return {
        nilOdds: parcels[loot].nilOdds,
        items: parcels[loot].itemList,
        normalOdds: parcels[loot].itemList.reduce((prev, cur) => prev + cur.odds, 0)
    }
}

function convertModel(model, id, isTypeB) {
    const epicBosses = ["Leviathan", "Kraken", "Genbu",
        "Kirin", "Alkyoneus", "Magnotaur",
        "Oni", "Hel", "TwinShadow"];
    const keyName = "name", keyAtk = "enemyAtk", keyMatk = "enemyMatk",
        keyDef = "enemyDef", keyMdef = "enemyMdef", keyHp = "enemyHp",
        keyEva = "enemyEva", keyAttackList = "attackList";
    let getter = key => {
        return isTypeB ? model[key + "B"] : model[key]
    };

    let getterLoot = num => isTypeB ? model["rewardB" + num] : model["rewardA" + num];

    let result = {
        name: getter(keyName),
        id: id + 1,
        atk: getter(keyAtk),
        matk: getter(keyMatk),
        def: getter(keyDef),
        mdef: getter(keyMdef),
        hp: getter(keyHp),
        attackList: getter(keyAttackList),
        eva: getter(keyEva),
        partySize: model.party ? model.questSize : 1,
        region: model.region,
        questType: model.questType,
        levelReq: model.levelReq,
        isRare: isTypeB,
        isEpic: epicBosses.indexOf(model.name) > -1,
        loot1: processLoot12(getterLoot(1), model.name),
        loot2: processLoot12(getterLoot(2), model.name),
        loot3: processLoot34(getterLoot(3), model.name),
        loot4: processLoot34(getterLoot(4), model.name),
    };

    result.nameLower = result.name.toLowerCase();


    return result;
}
console.log("loading quest data")
loadAndFilterData(data.data);
console.log("loading quest data done")

module.exports = data;