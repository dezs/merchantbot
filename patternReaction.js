var infoProvider = require("./classInfo/infoProvider")
var classes = require("./reactions/patternClasses")

var simplePattern = classes.simple;
var regexpPattern = classes.regexp;

var config = {

}

config.initHelp = function () {
    config.helpStr = config.patterns
        .filter(x => !x.isHidden)
        .map((x) => "**!" + x.command + "** - " + x.description);//;.join("\n")
    //config.helpStr = text;
}

// <:rog:352084398184726538> <:mage:352084688044556290> <:cler:352084688065789952> 

config.patterns = [
    new simplePattern("help", function () {
        return config.helpStr;
    }, "Get list of available commands"),
    new simplePattern("reload", function (message, messageContext) {
        var caller = messageContext.author;
        console.log("reload", caller.id, caller.username, caller.tag)
        if (caller.id == "248438423923326978") {
            var isSuccess = config.reloadDynamic();
            return isSuccess ? "reloaded" : "reload failed";
        }
        return "You have no power here!";
    }, "Reload bot commands", {
            isHidden: true
        }),
    new simplePattern("ping", "pong", "Test bot availability", {
        isHidden: true
    }),
    new simplePattern("roll", function () {
        return "random number (out of 100) is: " + Math.round(Math.random() * 100);
    }, "Roll random number"),
    new simplePattern("wiki", "http://merchantrpg.wikia.com/wiki/Guides/Game", "Merchant RPG wiki"),
    new simplePattern("db", "https://benzeliden.github.io/MerchantGameDB/", "Merchant RPG database"),
    new simplePattern("sim", "Merchant combat simulator (in dev) - https://merchant-calc.herokuapp.com/", "Link for Merchant Combat Simulator"),
]

//try min distance 3


config.getMessageReaction = function (message, messageContext) {
    var i = 0;
    var msg = null;
    while (msg == null && i < config.patterns.length) {
        if (config.patterns[i].checkReaction(message, messageContext)) {
            msg = config.patterns[i].react(message, messageContext)
        }
        i++;
    }

    return msg;
}

config.reloadDynamic = function () {
    var isSuccess = true
    try {
        var dynoPath = "./reactions/dynamicReactions";
        delete require.cache[require.resolve(dynoPath)]
        var dyno = require(dynoPath);
        dyno.toAdd.forEach(function (pattern) {
            var idx = config.patterns.findIndex(x => x.command == pattern.command);
            if (idx == -1) {
                config.patterns.push(pattern);
            } else {
                config.patterns[idx] = pattern;
            }
        })

        dyno.toRemove.forEach(function (command) {
            var idx = config.patterns.findIndex(x => x.command == command);
            if (idx > -1) {
                config.patterns.splice(index, 1)
            }
        });

        config.initHelp();
    } catch (ex) {
        console.error(ex)
        isSuccess = false
    }
    return isSuccess
}

config.reloadDynamic();

module.exports = config;