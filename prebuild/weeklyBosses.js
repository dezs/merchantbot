var weeklyBosses = [
  {
    "resultParams": [
      61
    ],
    "detectParams": [
      1506816000,
      604800,
      5443200
    ],
    "name": "Leviathan"
  },
  {
    "resultParams": [
      62
    ],
    "detectParams": [
      1507420800,
      604800,
      5443200
    ],
    "name": "Kirin"
  },
  {
    "resultParams": [
      63
    ],
    "detectParams": [
      1508025600,
      604800,
      5443200
    ],
    "name": "Oni"
  },
  {
    "resultParams": [
      64
    ],
    "detectParams": [
      1508630400,
      604800,
      5443200
    ],
    "name": "Hel"
  },
  {
    "resultParams": [
      65
    ],
    "detectParams": [
      1509235200,
      604800,
      5443200
    ],
    "name": "Alkyoneus"
  },
  {
    "resultParams": [
      66
    ],
    "detectParams": [
      1509840000,
      604800,
      5443200
    ],
    "name": "Kraken"
  },
  {
    "resultParams": [
      67
    ],
    "detectParams": [
      1510444800,
      604800,
      5443200
    ],
    "name": "Twin Shadow"
  },
  {
    "resultParams": [
      68
    ],
    "detectParams": [
      1511049600,
      604800,
      5443200
    ],
    "name": "Genbu"
  },
  {
    "resultParams": [
      69
    ],
    "detectParams": [
      1511654400,
      604800,
      5443200
    ],
    "name": "Magnotaur"
  },
  {
    "resultParams": [
      "speed",
      0.5
    ],
    "detectParams": [
      1512518400,
      86400,
      1209600
    ],
    "name": "Speedy Humpday"
  },
  {
    "resultParams": [
      "xp",
      2
    ],
    "detectParams": [
      1513123200,
      86400,
      1209600
    ],
    "name": "XP Humpday"
  },
  {
    "resultParams": [
      "speed",
      0.5
    ],
    "detectParams": [
      1512691200,
      172800,
      1209600
    ],
    "name": "Speed Weekend"
  },
  {
    "resultParams": [
      "xp",
      2
    ],
    "detectParams": [
      1512086400,
      172800,
      1209600
    ],
    "name": "XP Weekend"
  },
  {
    "resultParams": [
      70
    ],
    "detectParams": [
      1506816000,
      604800,
      2419200
    ],
    "name": "Galam"
  },
  {
    "resultParams": [
      72
    ],
    "detectParams": [
      1507420800,
      604800,
      2419200
    ],
    "name": "Odin"
  },
  {
    "resultParams": [
      71
    ],
    "detectParams": [
      1508025600,
      604800,
      2419200
    ],
    "name": "Cragus"
  },
  {
    "resultParams": [
      73
    ],
    "detectParams": [
      1508630400,
      604800,
      2419200
    ],
    "name": "Faust"
  }
]
module.exports = weeklyBosses