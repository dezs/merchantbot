
var fs = require("fs");
//use decompiled data.EventData.lu
//java -jar unluac.jar data.EventData.lu > data.EventData.lua

fs.access("data.EventData.lua", fs.R_OK, function(err){
    if (err!= null){
        console.log("Cannot find file data.EventData.lua", err);
        return;
    }
    var arr = [1,2,3,]
    var data = fs.readFileSync('data.EventData.lua', 'utf8');

    var lines = data.split("\n");

    var code = "var weeklyBosses = ["

    for (let i = 0; i < lines.length; i++){
        let line = lines[i];
        let matchTime = line.match(/{([\d]+), ([\d]+)}/);
        if (matchTime && matchTime[1] && matchTime[2]){
            let t = matchTime[1];
            code += "{ \n    time: " + t
        }else{
            let matchQuest = line.match(/{([\d]+)}/)
            if (matchQuest && matchQuest[1]){
                code += ", questId: " + matchQuest[1] + "\n},\n";
            }
        }
    }

    code = code + "]\nmodule.exports = weeklyBosses"
    
    fs.writeFileSync("weeklyBosses.js", code)
});
