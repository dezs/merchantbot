
var fs = require("fs");

fs.access("EventList.json", fs.R_OK, function (err) {
    if (err != null) {
        console.log("Cannot find file EventList.json", err);
        return;
    }
    var data = fs.readFileSync('EventList.json', 'utf8');

    var decoded = JSON.parse(data)

    var cleaned = []

    for (var i = 0; i < decoded.length; i++) {
        cleaned[i] = {
            resultParams: decoded[i].resultParams,
            detectParams: decoded[i].detectParams,
            name: decoded[i].name,
        }
    }

    var code = "var weeklyBosses = " + JSON.stringify(cleaned, null, "  ") + "\nmodule.exports = weeklyBosses"
    //console.log("test", code)

    fs.writeFileSync("weeklyBosses.js", code)
});
