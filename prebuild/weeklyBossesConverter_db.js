
var fs = require("fs");
//use decompiled data.EventData.lu
//java -jar unluac.jar data.EventData.lu > data.EventData.lua

fs.access("data.EventData.lua", fs.R_OK, function(err){
    if (err!= null){
        console.log("Cannot find file data.EventData.lua", err);
        return;
    }
    var arr = [1,2,3,]
    var data = fs.readFileSync('data.EventData.lua', 'utf8');

    var lines = data.split("\n");

    var code = "["
    var lastTime = -1
    var lastDuration = -1
    for (let i = 0; i < lines.length; i++){
        let line = lines[i];
        let matchTime = line.match(/{([\d]+), ([\d]+)}/);
        if (matchTime && matchTime[1] && matchTime[2]){
            lastTime = matchTime[1];
            lastDuration = matchTime[2];
        }else{
            let matchQuest = line.match(/{([\d]+)}/)
            if (matchQuest && matchQuest[1]){
                code += '{"resultParams":[' + matchQuest[1] + '],"detectParams":[' + lastTime + ',' + lastDuration + '],"name":" \\t\\t"},'
            }
        }
    }

    code = code.replace(/.$/,"]")
    
    fs.writeFileSync("EventList.json", code)
});
