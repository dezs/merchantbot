var classes = require("./patternClasses")
//delete require.cache[require.resolve("../classInfo/scalingProvider")]
var scalingProvider = require("../classInfo/scalingProvider")
//delete require.cache[require.resolve("../classInfo/infoProvider")]
//delete require.cache[require.resolve("../classInfo/infoResolver")]
var infoProvider = require("../classInfo/infoProvider")
//delete require.cache[require.resolve("./weekly/weeklyBosses")]
//delete require.cache[require.resolve("./weekly/weeklyProvider")]
var weeklyProvider = require("./weekly/weeklyProvider")
var serverData = require("../serverData")
var moment = require('moment');

delete require.cache[require.resolve("../luckCalculator/luckCalc")]
var luckCalc = require("../luckCalculator/luckCalc")


var simplePattern = classes.simple;
var regexpPattern = classes.regexp;

var additional = {
    toAdd: [],
    toRemove: []
}

additional.toAdd.push(
    new regexpPattern("^info[\\s]+(\\w+)[\\s]*(\\w*)[\\s]*(\\w*)", function (parts) {
        return infoProvider.getInfo(parts[1], parts[2], parts[3])
    }, "info <class name> <details>", "Get info about class. Example: `info mage`, `info rogue skills`")
)

additional.toAdd.push(new simplePattern("info",
    "Info command usage: ```!info <class name> <details>```",
    "helper", { isHidden: true }))

additional.toAdd.push(
    new regexpPattern("^scaling[\\s]+(\\w+)[\\s]*(\\w*)", function (parts) {
        return scalingProvider.getInfo(parts[1], parts[2])
    }, "scaling <class name>", "Get stats scaling for class.")
)

additional.toAdd.push(
    new regexpPattern("^luck[\\s]+(\\w+[\\s\\w]*)", function (parts) {
        return luckCalc.getLuck(parts[1])
    }, "luck <quest name>", "Show luck cap for quest.")
)

additional.toAdd.push(new simplePattern("luck",
    "Luck command usage: ```!luck <quest name>``` \n" +
    "Common information about the luck: http://merchantrpg.wikia.com/wiki/Guides/Game#LUCK"
    , "helper", { isHidden: true }))

additional.toAdd.push(
    new simplePattern("meta",
        function () {
            var emoticons = serverData.emoticons;
            return `__**Best setup:**__ 1-2 mages, 2 clerics, warrior, paladin, bard + any heroes your like
${emoticons.rogue} Rogue is good - high damage and evade.
${emoticons.mage} Mage - magical damage dealer, also silence is unique and important skill.
${emoticons.cleric} Cleric required for last bosses - at least one.
${emoticons.warrior} Warrior - pdef tank. With Taunt (DLC ability) it's useful in end game.
${emoticons.berserker} Berserker is strong physical damage dealer.
${emoticons.assassin} Assassin is true damage/physical damage dealer. Really good with t6 acc&crit gear.
${emoticons.paladin} Paladin is mdef tank. Aoe shield (DLC ability) makes end game content easy.
${emoticons.dk} Dark Knight is mix damage dealer. Need to test it with new skills
${emoticons.bard} Bard is support class with some useful buffs. Poem of Focus is OP with crit builded party`
        },
        "Describes current meta")
)

function dateToStr(num, strSingle, strMultiple) {
    return num + " " + (num % 10 == 1 ? strSingle : strMultiple);
}

additional.toAdd.push(
    new simplePattern("uptime", function (message, messageContext) {
        var now = moment();
        var startTime = moment(messageContext.client.readyTimestamp)
        var dateDiff = moment.duration(now.diff(startTime));
        //console.log("uptime diff", dateDiff);
        var sec = dateDiff.seconds();
        var min = dateDiff.minutes();
        var hour = dateDiff.hours();
        var days = dateDiff.asDays();

        var result = "";
        if (days >= 1) {
            result = dateToStr(Math.floor(days), "day", "days") + " ";
        }
        result += dateToStr(hour, "hour", "hours") + " ";
        result += dateToStr(min, "minute", "minutes") + " ";
        result += dateToStr(sec, "second", "seconds");

        return "Uptime: " + result;
    }, "Shows bot uptime"));

additional.toAdd.push(new simplePattern("weekly",
    function () {
        let info = weeklyProvider.getBossInfo();
        let bosses = info.bosses;
        let result = {
            color: 0x8000ff,
            title: "__**Weekly Bosses:**__ ",
        };//"__**Weekly Bosses:**__ ";
        let fields = []
        fields.push({
            name: "**Current week we have " + info.boost + " boost!** ",
            value: "Boost applied on Wednesday and weekend"
        })
        for (let i = 0; i < bosses.length; i++) {
            let date = bosses[i].date.format("MMMM D")
            let val = ""
            let questIds = bosses[i].questIds
            for (let j = 0; j < questIds.length; j++) {
                let quest = luckCalc.getQuestById(questIds[j])
                let name = quest == null ? "<undefined>" : quest.name
                let region = quest == null ? "" : ` Region ${quest.region}`
                val += "**[" + name + "](https://benzeliden.github.io/MerchantGameDB/BETA/#!/quests/" + encodeURIComponent(name) + ")** - " + region
                if (j != questIds.length - 1) {
                    val += "\n"
                }
            }
            fields.push({
                name: date,
                value: val
            })
        }

        result.fields = fields
        //console.log(result)
        return {
            content: "Weekly",
            embed: result
        };
    },
    //Why weekly bosses dissapear? Thats because schedule was hardcoded in game.
    "Weekly bosses schedule"))

additional.toAdd.push(
    new regexpPattern("^questbyid[\\s]+(\\d+)", function (parts) {
        return luckCalc.getQuestName(parts[1] | 0)
    }, "questbyid <id>", "Search quest by id.", {
            isHidden: true
        })
)
additional.toAdd.push(new simplePattern("next update",
    "Merchant: The Frozen Tome \n(see more: https://www.reddit.com/r/MerchantRPG/comments/7gydiq/merchant_2017_roadmap/)",
    "Next update information", {
        isHidden: true
    })
)

additional.toAdd.push(
    new simplePattern("cleanup", function (message, messageContext) {
        var caller = messageContext.author;
        console.log("cleanup", caller.id, caller.username, caller.tag)
        if (caller.id == "248438423923326978") {
            try {
                let channel = messageContext.channel
                if (!messageContext.guild || channel.constructor.name != "TextChannel") {
                    return "Cannot cleanup this channel"
                }
                return channel
                    .fetchMessages({ limit: 50 })
                    .then(function (list) {
                        var filterDate = moment().subtract(7, "d");


                        let filtered = list.filter(msg => (msg.author == serverData.bot.user || msg.author == caller)
                            && filterDate.isBefore(msg.createdAt));

                        if (filtered.size < 2) {
                            return null;
                        }

                        //let test = filtered.map(x => x.cleanContent).reduce((accum, cur) => accum + cur + "\n", "")
                        //messageContext.reply(`Found some messages ${filtered.size}\n${test}`)
                        return channel.bulkDelete(filtered)
                    }, err => {
                        console.log("Error", err)
                        return "Error"
                    })
                    .then(list => {
                        if (list == null) {
                            return `Nothing to delete`
                        }
                        return `Deleted ${list.size} messages`
                    }, err => {
                        console.log("Error", err)
                        return "Error"
                    });

            } catch (err) {
                console.log(err);
                return "Error"
            }
        }
        return "You have no power here!";

    }, "Cleanup channel from bot messages", { isHidden: true })
)
additional.toAdd.push(new simplePattern("shrug",
    "¯\\_(ツ)_/¯", "¯\\_(ツ)_/¯", { isHidden: true })
)
additional.toAdd.push(new simplePattern("backup",
    "https://www.reddit.com/r/MerchantRPG/comments/806dgb/weekly_backup_thread/",
    "Weekly backup thread", { isHidden: true })
)
additional.toAdd.push(new simplePattern("sticky",
    "https://www.reddit.com/r/MerchantRPG/comments/806djl/merchant_mega_sticky_thread/",
    "Merchant Mega Sticky Thread", { isHidden: true })
)

additional.toAdd.push(new simplePattern("feature",
    "https://www.reddit.com/r/MerchantRPG/comments/806cos/merchant_feature_suggestions/",
    "Link to feature suggestions thread"))

additional.toAdd.push(new simplePattern("bugs",
    `Wiki - http://merchantrpg.wikia.com/wiki/Bugs
Reddit Bug Mega Thread - https://www.reddit.com/r/MerchantRPG/comments/7p0bi3/bug_report_mega_thread/`
    , "Known bugs"))

additional.toAdd.push(new simplePattern("bis",
    "https://docs.google.com/spreadsheets/d/1iKaOHAFV2sIxee9BBXFo6QUnXbxKwphgIWcifhU-bZk",
    "Best-In-Slot equipment"))

additional.toAdd.push(new simplePattern("beta",
    "Merchant Android Beta: https://play.google.com/apps/testing/bearface.games.merchant",
    "Android Beta", { isHidden: true }))

module.exports = additional;
