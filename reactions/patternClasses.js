class simplePattern {
    constructor(command, reaction, description, options) {
        this.command = command;
        this.reaction = reaction;
        this.description = description;
        if (options){
            this.isHidden = options.isHidden;
        }
    }
    checkReaction(message, messageContext) {
        if (message.toLowerCase() == this.command) {
            return true;
        }
        return false;
    }
    react(message, messageContext) {
        if (typeof this.reaction == "function") {
            return this.reaction(message, messageContext);
        }
        return this.reaction;
    }
}

class regexpPattern {
    constructor(pattern, reaction, command, description, options) {
        this.pattern = new RegExp(pattern);
        this.reaction = reaction;
        this.command = command;
        this.description = description;
        if (options){
            this.isHidden = options.isHidden;
        }
    }
    checkReaction(message, messageContext) {
        //console.log("check", message, this.pattern.test(message))
        if (this.pattern.test(message.toLowerCase())) {
            return true;
        }
        return false;
    }
    react(message, messageContext) {
        if (typeof this.reaction == "function") {
            //console.log("match", message.match(this.pattern))
            return this.reaction(message.toLowerCase().match(this.pattern), messageContext);
        }
        return this.reaction;
    }
}

module.exports = {
    simple: simplePattern,
    regexp: regexpPattern
}