var data = require("./weeklyBosses")
var moment = require('moment');

var provider = {}

provider.getBossInfo = function () {
    let topCount = 5
    let currentTimestamp = moment().unix();

    let res = [];
    let boosts = [];
    for (let i = 0; i < data.length; i++) {
        var boss = data[i]
        var isBoost = boss.resultParams.length > 1
        var startTimestamp = boss.detectParams[0];
        var duration = boss.detectParams[1];
        var endTimestamp = startTimestamp + duration;
        if (boss.detectParams[2] !== undefined) {
            var period = Math.floor((currentTimestamp - startTimestamp) / boss.detectParams[2]) * boss.detectParams[2]
            if ((endTimestamp + period) < currentTimestamp) {
                period = period + boss.detectParams[2]
            }
            startTimestamp = startTimestamp + period
            endTimestamp = endTimestamp + period
        }
        if (isBoost) {
            boosts.push({
                startTimestamp: startTimestamp,
                type: boss.resultParams[0]
            })
        } else {
            var dateStart = moment.unix(startTimestamp)
            res.push({
                date: dateStart,
                startTimestamp: startTimestamp,
                name: boss.name,
                questId: boss.resultParams[0]
            })
        }
    }
    res.sort((a, b) => a.startTimestamp - b.startTimestamp);
    let bosses = []
    let idx = 0
    while (idx < res.length && (bosses.length <= topCount)) {
        let lastBoss = bosses[bosses.length - 1]
        if (lastBoss && lastBoss.startTimestamp == res[idx].startTimestamp) {
            lastBoss.questIds.push(res[idx].questId)
        } else {
            bosses.push({
                questIds: [res[idx].questId],
                date: res[idx].date,
                startTimestamp: res[idx].startTimestamp
            })
        }
        idx++;
    }

    boosts.sort((a, b) => a.startTimestamp - b.startTimestamp);
    return {
        bosses: bosses,
        boost: boosts[0].type
    };
}

module.exports = provider