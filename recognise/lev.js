var levenshtein = require('fast-levenshtein');

var alg = {}

alg.findClosest = function (stringValue, arrayToSearch, minForResult, getter) {
    var min = minForResult || 5;

    var minDistance = 99999;
    var idx = -1;
    for (var i = 0; i < arrayToSearch.length; i++) {
        var val = getter ? getter(arrayToSearch[i]) : arrayToSearch[i]
        var distance = levenshtein.get(stringValue, val);
        if (distance < minDistance) {
            minDistance = distance;
            idx = i;
        }
    }
    console.log("min distance", minDistance, idx, getter ? getter(arrayToSearch[idx]) : arrayToSearch[idx]);
    if (minDistance < 5) {
        return {
            match: arrayToSearch[idx],
            distance: minDistance
        };
    }
    return {
        match: null,
        distance: minDistance
    };
}

alg.replaceSynonim = function (name, synonims) {
    var synonim = synonims[name]
    if (synonim) {
        return synonim;
    }
    return name;
}


module.exports = alg;