var data = {}

data.init = function (bot) {
    data.bot = bot

    var emoticons = {}
    emoticons.rogue = bot.emojis.cache.find(c => c.name == "rogue");
    emoticons.mage = bot.emojis.cache.find(c => c.name == "mage");
    emoticons.cleric = bot.emojis.cache.find(c => c.name == "cleric");
    emoticons.warrior = bot.emojis.cache.find(c => c.name == "warrior");
    emoticons.berserker = bot.emojis.cache.find(c => c.name == "zerk");
    emoticons.assassin = bot.emojis.cache.find(c => c.name == "assassin");
    emoticons.paladin = bot.emojis.cache.find(c => c.name == "paladin");
    emoticons.dk = bot.emojis.cache.find(c => c.name == "darkknight");
    emoticons.bard = bot.emojis.cache.find(c => c.name == "bard");
    data.emoticons = emoticons

    data.channels = {}
    data.channels.intro = bot.channels.cache.find(c => c.name == 'introductions')
    data.channels.questionsChannel = bot.channels.cache.find(c => c.name == 'questions')
    data.channels.arg = bot.channels.cache.find(c => c.name == 'arg-2018')
    data.channels.botPlayground = bot.channels.cache.find(c => c.name == 'bot-playground')
    data.channels.merchantDb = bot.channels.cache.find(c => c.name == 'merchant-db')
    data.channels.merchantCalc = bot.channels.cache.find(c => c.name == 'merchant-calc')
    data.channels.weMakeGames = bot.channels.cache.find(c => c.name == 'we-make-games')
    data.channels.botTest = bot.channels.cache.find(c => c.name == 'bot-test')
    if (!data.channels.botTest) {
        console.log("bot-test channel not found")
    }
    data.restrictedChannels = [
        data.channels.intro,
        data.channels.merchantDb,
        data.channels.merchantCalc,
        data.channels.weMakeGames,
        data.channels.arg,
    ]

    for (var i = 0; i < data.restrictedChannels.length; i++) {
        if (!data.restrictedChannels[i]) {
            console.log("channel not found", i)
        }
    }
}

module.exports = data