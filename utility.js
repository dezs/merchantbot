var utility = {}

var patterns = require("./patternReaction")

//0 - false
//1 - @bot
//2 - @everyone
utility.getMentionState = function (message, bot) {
    var mentions = message.mentions
    if (mentions.everyone) {
        return 2;
    }

    var mention = mentions.users.find(u => u.username == bot.user.username)
    if (mention != null) {
        return 1;
    }
    return 0;
}

var helloMessages = [
    { m: "Hello", w: 3 },
    { m: "Whats up", w: 1 },
    { m: "Nice to meet you", w: 2 },
    { m: "Hi", w: 1 },
    { m: "I greet you", w: 1 },
    { m: "Greetings", w: 1 },
];
var total = helloMessages.reduce((prev, cur) => prev + cur.w, 0)
console.log("total", total)

utility.getHelloMessage = function () {
    var w = Math.random() * total;
    var i = 0;
    while (i < helloMessages.length && helloMessages[i].w < w) {
        w -= helloMessages[i].w
        i++;
    }
    console.log("asdasd", i, w)
    return helloMessages[i].m;
}

utility.resolveMentionMessage = function (messageCorrected, messageContext, isChannelRestricted) {

    console.log("resolve mention", messageCorrected)
    var msg = patterns.getMessageReaction(messageCorrected, messageContext)

    if (msg != null) {
        if (isChannelRestricted) {
            return {
                isRestricted: true
            }
        }
        return {
            msg: msg
        }
    }

    //if (messageCorrected.endsWith("PM")) {
    //    return {
    //        msg: message,
    //        doPM: true
    //    }
    //}

    var words = messageCorrected.match(/[a-z'\-]+/gi);
    var greet = words.indexOf("hello") > -1 || words.indexOf("hi") > -1 || words.indexOf("greetings") > -1
    if (greet) {

        return {
            msg: utility.getHelloMessage()
        }
    }

    return {}
}

utility.resolveCommonMessage = function (message, messageContext, isChannelRestricted) {
    if (message.startsWith("!")) {
        if (isChannelRestricted) {
            return {
                isRestricted: true
            }
        }
        var msg = patterns.getMessageReaction(message.substring(1).trim(), messageContext)

        if (msg != null) {
            if (msg.constructor.name == "Promise"){
                return {
                    promise: msg
                }
            }
            return {
                msg: msg
            }
        }

        return {
            msg: "unknown bot command"
        }
    }

    return null;
}

module.exports = utility